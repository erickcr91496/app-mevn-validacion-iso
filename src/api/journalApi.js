
import axios from 'axios'


const journalApi = axios.create({
   // baseURL: 'http://localhost:8000/api',
    baseURL: 'https://app-server-mevn.herokuapp.com/api',
   headers : {
    'x-token': localStorage.getItem('token'),
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*" 
}

})





// console.log( process.env.NODE_ENV ) // TEST durante testing, 

export default journalApi


