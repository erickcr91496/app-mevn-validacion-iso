
import axios from 'axios'


// const authApi = axios.create({
//     baseURL: 'https://identitytoolkit.googleapis.com/v1/accounts',
//     params: {
//         key: 'AIzaSyDnGKkKckAshxePqc2vx4BDHh2zO9VrZwc'
//     }
// })

const authApi = axios.create({
   //baseURL: 'http://localhost:8000/api',

    baseURL: 'https://app-server-mevn.herokuapp.com/api',
    headers: {
        "Content-Type": "application/json",
    }

})
// const authApi = axios.create({
//     baseURL: 'http://localhost:8000/api',
//     headers: {
//         "Content-Type": "application/json",
//     }

// })
// console.log( process.env.NODE_ENV ) // TEST durante testing, 

export default authApi


