import authApi from "@/api/authApi";

// export const myAction = async ({ commit }) => {

// }

export const createUser = async ({ commit }, user) => {
  const { nombre, correo, password } = user;

  try {
    const { data } = await authApi.post("/usuarios", {
      nombre,
      correo,
      password,
    });
    console.log(data);

    const { usuario } = data;

    //  await authApi.post(':update', { displayName: name, idToken, refreshToken })

    delete user.password;

    return { ok: true };
  } catch (error) {
    return { ok: false, message: "Error register" };
  }
};

export const signInUser = async ({ commit }, user) => {
  const { correo, password } = user;

  try {
    const { data } = await authApi.post("/auth/login", {
      correo,
      password,
      token,
    });
    const { usuario, token } = data;
    console.log('Usuario id: '+usuario.uid)

    user.name = usuario.nombre;
    user.id = usuario.uid

    commit("loginUser", { user, token });

    return { ok: true };
  } catch (error) {
    return { ok: false, message: "Error login" };
  }
};

export const checkAuthentication = async ({ commit }) => {
  const idToken = localStorage.getItem("token");

  if (!idToken) {
    commit("logout");
    return { ok: false, message: "No hay token" };
  }

  try {

    const {data}=   await authApi.get('/auth', {
      headers: {  'x-token': idToken ,
      'Content-Type': 'application/json'}

    } )

        const { usuario, token } = data;

 
     const user = {
        name: usuario.nombre,
      correo:usuario.correo,
      id: usuario.uid
   }
  //   console.log('Nombre es: '+user.name)

    commit('loginUser', { user, token })

   return { ok: true };
    
  } catch (error) {
    commit("logout");
    return { ok: false, message: error.response.data.error.message };
  }
};
