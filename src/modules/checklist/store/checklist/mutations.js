
//  export const myAction = ( state ) => {

// }

export const CREATE_EVALUACION = ( state , data) => {
    state.evaluaciones = data
    state.isLoading = false
}


export const GET_EVALUACIONES = ( state , data) => {
    state.evaluaciones = data
    state.isLoading = false

}


export const GET_EVALUACIONES_REQUEST = ( state ) => {
    state.isLoading = true
}
export const GET_EVALUACIONES_SUCCESS = ( state ) => {
    state.isLoading = false
}

export const GET_ONE_EVALUACION = ( state,data ) => {
    state.evaluacion = data
    state.isLoading = false

}