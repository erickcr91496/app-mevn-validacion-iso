import journalApi from "./../../../../api/journalApi";
import authApi from "./../../../../api/authApi";
// export const myAction = async ({ commit }) => {

// }

export const fetchEvaluaciones = async ({ commit }) => {
  try {
    // commit('GET_EVALUACIONES_REQUEST')

    let evaluaciones = await journalApi.get("/evaluaciones");
    commit("GET_EVALUACIONES", evaluaciones.data.evaluacion);
    console.log(evaluaciones);
    return { ok: true };
  } catch (error) {
    console.log(error);
    return { ok: false, message: "Error " };
  }
};
export const fetchEvaluacionesByUser = async ({ commit }, id) => {
  try {
    commit("GET_EVALUACIONES_REQUEST");

    let evaluaciones = await journalApi.get("/evaluaciones/" + id);
    commit("GET_EVALUACIONES", evaluaciones.data.evaluaciones);

    // console.log(evaluaciones.data.evaluaciones)
    return { ok: true };
  } catch (error) {
    console.log(error);
    return { ok: false, message: "Error " };
  }
};

export const getUsuario = async (id) => {
  try {
    console.log("llega accions getUsuario");
    //const {data} = await journalApi.get('/usuarios/61eb55b5130e4787345c85bf',  {

    let data = await journalApi.get("/usuarios/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    console.log("!!!Obteniendo el usuario: " + JSON.stringify(data, null, " "));
    return JSON.stringify(data.usuario.nombre, null, " ");
  } catch (error) {
    console.log(error);
    return { ok: false, message: "Error " + error };
  }
};


export const createEvaluacion = async ({ commit }, evaluacion) => {

  const {
    nombre,
    autor,
    check1,
    check2,
    check3,
    check4,
    check5,
    check6,
    check7,
    check8,
    check9,
    check10,
    check11,
    check12,
    check13,
    check14,
    check15,
    id
  } = evaluacion;
  console.log('obtener id del actions: '+id)

  const idToken = localStorage.getItem("token");
  let cont = 0;

  try {
    commit('GET_EVALUACIONES_REQUEST')

    //      -----------------------------CONTAR CHECKS----------------
    if (check1) cont = cont + 1;
    if (check2) cont = cont + 1;
    if (check3) cont = cont + 1;
    if (check4) cont = cont + 1;
    if (check5) cont = cont + 1;

    if (check6) cont = cont + 1;
    if (check7) cont = cont + 1;
    if (check8) cont = cont + 1;
    if (check9) cont = cont + 1;
    if (check10) cont = cont + 1;

    if (check11) cont = cont + 1;
    if (check12) cont = cont + 1;
    if (check13) cont = cont + 1;
    if (check14) cont = cont + 1;
    if (check15) cont = cont + 1;
    const calcularPorcentaje = () => {
      let res = (cont * 100) / 15;
      return res.toFixed(2);
    };
    const valor = calcularPorcentaje();
    console.log("CALCULAR PORCENTAJE" + valor);
    // const {data } =
    let datos = await journalApi.post(
      "/proyectos",
      {
        nombre,
        autor,
        check1,
        check2,
        check3,
        check4,
        check5,
        check6,
        check7,
        check8,
        check9,
        check10,
        check11,
        check12,
        check13,
        check14,
        check15,
        valor,
      },
      {
        headers: { "x-token": idToken },
      }
    );

    commit("CREATE_EVALUACION", datos.data);
    let evaluaciones = await journalApi.get('/evaluaciones/')
    commit('GET_EVALUACIONES',evaluaciones.data.evaluaciones)

    return { ok: true,  message: "Creado " };
  } catch (error) {
    console.log(error);
    return { ok: false, message: "Error " };
  }
};
export const updateEvaluacion = async ({ commit }, evaluacion) => {
  const {
    proyecto,
    check1,
    check2,
    check3,
    check4,
    check5,
    check6,
    check7,
    check8,
    check9,
    check10,
    check11,
    check12,
    check13,
    check14,
    check15,
    id
  } = evaluacion;

  console.log('id para actualizar en accions:'+id)
  const {nombre, autor} = proyecto
  console.log("proyecto.nombre: "+nombre)
  console.log("proyecto.autor: "+autor)
  const idToken = localStorage.getItem("token");
  let cont = 0;

  try {
    commit('GET_EVALUACIONES_REQUEST')

    //      -----------------------------CONTAR CHECKS----------------
    if (check1) cont = cont + 1;
    if (check2) cont = cont + 1;
    if (check3) cont = cont + 1;
    if (check4) cont = cont + 1;
    if (check5) cont = cont + 1;

    if (check6) cont = cont + 1;
    if (check7) cont = cont + 1;
    if (check8) cont = cont + 1;
    if (check9) cont = cont + 1;
    if (check10) cont = cont + 1;

    if (check11) cont = cont + 1;
    if (check12) cont = cont + 1;
    if (check13) cont = cont + 1;
    if (check14) cont = cont + 1;
    if (check15) cont = cont + 1;
    const calcularPorcentaje = () => {
      let res = (cont * 100) / 15;
      return res.toFixed(2);
    };
    const valor = calcularPorcentaje();
    console.log("CALCULAR PORCENTAJE" + valor);
    // const {data } =
    let datos = await journalApi.put(
      "/proyectos/"+id,
      {
        proyecto:{
          nombre,
          autor
        },
        check1,

        check2,
        check3,
        check4,
        check5,
        check6,
        check7,
        check8,
        check9,
        check10,
        check11,
        check12,
        check13,
        check14,
        check15,
        valor
      },
      {
        headers: { "x-token": idToken ,
        "Access-Control-Allow-Origin": "*" ,
        'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT, DELETE'
        },
      }
    );
    console.log('data print: '+datos)

    //commit("CREATE_EVALUACION", datos.data);
    let evaluaciones = await journalApi.get('/evaluaciones/')
  console.log(evaluaciones)
     commit('GET_EVALUACIONES_SUCCESS')

    return { ok: true,  message: "Creado " };
  } catch (error) {
    console.log(error);
    commit('GET_EVALUACIONES_SUCCESS')

    return { ok: false, message: "Error " };
  }

}

export const fetchEvaluacion = async ({ commit }, id) => {
  try {
    //commit("GET_EVALUACIONES_REQUEST");

    let evaluaciones = await journalApi.get("/evaluaciones/detail/" + id)
    let res =evaluaciones.data.evaluacion
    //commit("GET_ONE_EVALUACION", evaluaciones.data.evaluacion);
    return res;
    // return { ok: true };
  } catch (error) {
    console.log(error);
    return { ok: false, message: "Error " };
  }


}


