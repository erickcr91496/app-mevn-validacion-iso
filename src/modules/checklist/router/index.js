

export default {

    name: 'checklist',
    component: () => import(/* webpackChunkName: "daybook" */ '@/modules/checklist/layouts/CheckListLayout.vue'),
    children: [
        {
            path: '',
            name: 'no-project',
            component: () => import(/* webpackChunkName: "daybook-no-entry" */ '@/modules/checklist/views/NoEntrySelected.vue'),
        },
        {
            path: ':id',
            name: 'new-project',
            component: () => import(/* webpackChunkName: "daybook-no-entry" */ '@/modules/checklist/views/EntryView.vue'),
            props: ( route ) => {
                return {
                    id: route.params.id
                }
            }
        },
        {
            path: 'detail/:id',
            name: 'detail',
            component: () => import(/* webpackChunkName: "daybook-no-entry" */ '@/modules/checklist/components/CheckReport.vue'),
            props: ( route ) => {
                return {
                    id: route.params.id
                }
            }
        }
    ]

}