import { computed } from 'vue'
import { useStore } from 'vuex'


const useCheck = () => {
    
    const store = useStore()

    const createEvaluacion= async( evaluacion) => {
        const resp = await store.dispatch('checklist/createEvaluacion', evaluacion)
        return resp
    }

    const getUsuario = async (id) =>{
        const resp = await store.dispatch('checklist/getUsuario',id)
        console.log('Respuesta del metodo accion: '+resp)
        return resp
    }
    const fetchEvaluaciones= async(  ) => {
        const resp = await store.dispatch('checklist/fetchEvaluaciones' )
        return resp
    }
    const fetchEvaluacionesByUser= async(  id) => {
        const resp = await store.dispatch('checklist/fetchEvaluacionesByUser',id )
        return resp
    }
    const updateEvaluacion = async (evaluacion) => {
        const resp = await store.dispatch('checklist/updateEvaluacion',evaluacion )
        return resp
    }
    const fetchEvaluacion = async (id) => {
        const resp = await store.dispatch('checklist/fetchEvaluacion',id )
        return resp
    }
    return {
        createEvaluacion,
        fetchEvaluacion,
        updateEvaluacion,
        getUsuario,
        evaluaciones: computed(()=> store.getters['checklist/evaluaciones']),
        evaluacion: computed(()=> store.getters['checklist/evaluacion']),
        
        userId: computed(()=> store.getters['auth/userId']),
        fetchEvaluaciones,
        fetchEvaluacionesByUser

        // authStatus: computed(()=> store.getters['auth/currentState']),
        // username: computed(()=> store.getters['auth/username'])
    }
}

export default useCheck