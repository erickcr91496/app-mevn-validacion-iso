import { createStore } from 'vuex'

import auth from '../modules/auth/store'
import journal from '../modules/daybook/store/journal'
import checklist from '../modules/checklist/store/checklist'

const store = createStore({
    modules: {
        auth,
        journal,
        checklist
    }
})




export default store